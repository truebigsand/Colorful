# Colorful

#### 介绍
Colorful 是一个用来在Windows或Linux上输出彩色文本的C++库
#### 使用方法
使用SetColor函数设置文字颜色后输出
#### 提示
可以使用头文件内定义好的颜色和样式（仅限Linux）宏